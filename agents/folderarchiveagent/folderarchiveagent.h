/*
  Copyright (c) 2013 Montel Laurent <montel@kde.org>

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef FOLDERARCHIVEAGENT_H
#define FOLDERARCHIVEAGENT_H

#include <akonadi/agentbase.h>
class FolderArchiveManager;
class FolderArchiveAgent : public Akonadi::AgentBase, public Akonadi::AgentBase::ObserverV3
{
    Q_OBJECT
public:
    explicit FolderArchiveAgent(const QString &id);
    ~FolderArchiveAgent();

    void setEnableAgent(bool b);
    bool enabledAgent() const;

    void showConfigureDialog(qlonglong windowId = 0);

    void archiveItems(const QList<qlonglong> &itemIds, const QString &instanceName );
    void archiveItem(qlonglong itemId);

    void collectionRemoved( const Akonadi::Collection &collection );

    void debugCache();

public Q_SLOTS:
    void configure( WId windowId );

private:
    FolderArchiveManager *mFolderArchiveManager;
};

#endif // FOLDERARCHIVEAGENT_H
